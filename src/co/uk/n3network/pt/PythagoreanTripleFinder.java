package co.uk.n3network.pt;

import java.util.LinkedList;

/**
 * @author Chandler Newman
 */
public class PythagoreanTripleFinder {
	private LinkedList<PythagoreanTriple> triples;
	private int started, done;
	
	public void findAuto(int stop, int threads){
		int tot = stop + ( (stop % threads) == 0 ? 0 : (threads-(stop % threads)));
		int pt = tot/threads;
		System.out.println("Searchng 1->"+tot);
		find(threads, pt);
	}
	
	public void find(int threads, int perThread){
		triples = new LinkedList<PythagoreanTriple>();
		
		started = threads;
		done = 0;
		
		for(int i = 0; i < threads; i++){
			new PythagoreanTripleThread(this, (i*perThread)+1, 1, (i*perThread)+perThread+1, (threads*perThread)+1).start();
		}	
	}
	
	public void addResult(PythagoreanTriple t){
		synchronized (PythagoreanTripleFinder.class) {
			triples.add(t);
		}
	}
	
	public int ammountFound(){
		return triples.size();
	}
	
	public boolean isDone(){
		return started == done;
	}

	public int getStarted() {
		return started;
	}
	
	public int getDone(){
		return done;
	}

	public void markDone() {
		done++;
	}

	public LinkedList<PythagoreanTriple> getTriples() {
		return triples;
	}
	
}
