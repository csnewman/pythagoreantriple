package co.uk.n3network.pt;

/**
 * @author Chandler Newman
 */
public class PythagoreanTriple {
	private int a, b, c;

	public PythagoreanTriple(int a, int b, int c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public int getA() {
		return a;
	}

	public int getB() {
		return b;
	}

	public int getC() {
		return c;
	}
}
