package co.uk.n3network.pt;

/**
 * @author Chandler Newman
 */
public class PythagoreanTripleThread extends Thread {
	private PythagoreanTripleFinder finder;
	private int start, rel, stop, st;

	public PythagoreanTripleThread(PythagoreanTripleFinder finder, int start,
			int rel, int stop, int st) {
		this.finder = finder;
		this.start = start;
		this.rel = rel;
		this.stop = stop;
		this.st = st;
	}

	@Override
	public void run() {
		for(int x = start; x < stop; x++){
			int xx = x * x;
			int y = x + 1;
			int z = y + 1;
			while(z <= st){
				int zz = xx + (y * y);
				while (z * z < zz){
					z++;
				}
				if(z * z == zz && z <= st){
					finder.addResult(new PythagoreanTriple(x, y, z));
				}
				y++;
			}
		}
		
		finder.markDone();
	}
}	
	/*Old method for calculating
	 * Slow and does not work well in multithread instances 
	 */																				
	/*for (int a = start; a < stop - 2; a++) {
		for (int b = rel + 1; b < st - 1; b++) {
			if (gcd(a, b) == 1) {
				for (int c = b + 1; c < st; c++) {
					if ((a < b) && ((a * a) + (b * b) == (c * c))) {
						System.out.println("FOUND: " + a + " " + b + " " + c);
						finder.addResult(new PythagoreanTriple(a, b, c));
					}
				}
			}
		}
	}
	public int gcd(int a, int b) {
		int c;
		while (b != 0) {
			c = a % b;
			a = b;
			b = c;
		}
		return a;
	}*/