package co.uk.n3network.pt;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Chandler Newman
 */
public class Tester {
	public static void main(String[] args){
		PythagoreanTripleFinder f = new PythagoreanTripleFinder();
		f.findAuto(1000, 50);
		
		//Checks if we are done else wait 500ms before checking
		while(!f.isDone()){
			try {
				Thread.yield();
				Thread.sleep(500);
			} catch (InterruptedException e) {}
		}
		
		System.out.println("Done: "+f.ammountFound()+" triples found!");
		
		//Outputs triples to text file
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File("out.txt")));
			for(PythagoreanTriple t : f.getTriples()){
				writer.write("A("+t.getA()+") B("+t.getB()+") C("+t.getC()+")\n");
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
